package ictgradschool.web.lab10.exercise01;

import ictgradschool.web.lab10.exercise05.UserDetails;
import ictgradschool.web.lab10.utilities.HtmlHelper;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;

public class UserDetailsServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");

        PrintWriter out = response.getWriter();

        // Header stuff
        out.println(HtmlHelper.getHtmlPagePreamble("Web Lab 10 - Sessions"));



        out.println("<a href=\"index.html\">HOME</a><br>");

        out.println("<h3>Data entered: </h3>");
        //TODO - add the firstName, lastName, city and country  that were entered into the form to the list below
        //TODO - add the parameters from the form to session attributes

        HttpSession sess = request.getSession(true);

        String firstname = request.getParameter("firstname");
        String lastname = request.getParameter("lastname");
        String city = request.getParameter("city");
        String country = request.getParameter("country");

        UserDetails user= new UserDetails(firstname, lastname, city, country);
        sess.setAttribute("UserDetails", user);



        Cookie f = new Cookie("firstname", firstname);
        Cookie l = new Cookie("lastname", lastname);
        Cookie ct = new Cookie("city", city);
        Cookie c = new Cookie("country", country);

        response.addCookie(f);
        response.addCookie(l);
        response.addCookie(ct);
        response.addCookie(c);

            out.println("<ul>");
        out.println("<li>First Name: "+ firstname + "</li>");
        out.println("<li>Last Name:" + lastname+ "</li>");
        out.println("<li>Country:" + country + "</li>");
        out.println("<li>City:" + city + "</li>");
        out.println("</ul>");

        out.println(HtmlHelper.getHtmlPageFooter());
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Process POST requests the same as GET requests
        doGet(request, response);
    }
}
