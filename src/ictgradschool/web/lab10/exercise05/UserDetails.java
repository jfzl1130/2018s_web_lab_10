package ictgradschool.web.lab10.exercise05;

public class UserDetails {
    String firstname;
    String lastname;
    String city;
    String country;


    public UserDetails(String firstname, String lastname, String city, String country) {

        this.firstname = firstname;
        this.lastname = lastname;
        this.city = city;
        this.country = country;
    }


public String getFirstname(){
        return firstname;
}

public String getLastname(){

        return lastname;
}

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }
}


